package com.jeesuite.bestpl.dao.mapper;

import com.jeesuite.bestpl.dao.entity.PostCategoryEntity;
import tk.mybatis.mapper.common.BaseMapper;

public interface PostCategoryEntityMapper extends BaseMapper<PostCategoryEntity> {
}